package com.example.cameraexample;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {
	Button cameraButton;
	ImageView imageView;
	Bitmap bitmap;
	int CAMERA_PIC_REQUEST = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		cameraButton = (Button) findViewById(R.id.cameraButton);
		imageView = (ImageView) findViewById(R.id.imageView);
		cameraButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cameraButton:
			Intent intent = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent, CAMERA_PIC_REQUEST);
			break;

		default:
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
			
			bitmap = (Bitmap) data.getExtras().get("data");

			imageView.setImageBitmap(bitmap);
		} else if (resultCode == Activity.RESULT_CANCELED) {
			imageView.setBackgroundResource(R.drawable.logo);
		}
	}

	@Override
	protected void onPause() {

		super.onPause();
		imageView.setImageBitmap(bitmap);
	}

	@Override
	public void onBackPressed() {

		super.onBackPressed();
		

	}

}
